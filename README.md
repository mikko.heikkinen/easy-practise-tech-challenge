# Run the challenge

1. Run `composer update`, `npm install`, `npm run development`, `php artisan key:generate` and `php artisan serve`

## Fixes

**Solve this first:**
- [x] (BUG) I created some seeders that you can run with `php artisan db:seed`, but it gives an error. Can you make it work?

**And these in any order:**
- [x] (BUG) For some reason, the client bookings are not showing up in the front-end. Can you fix that?
- [x] (BUG) The list of bookings displayed on a client page has unformatted dates. Can you make sure they look something like this: `Monday 19 January 2020, 14:00 to 15:00`
- [x] (FEATURE) Currently, any logged-in user can view all of the system's clients, including those created by other users. Users are obviously not happy with that. Can you make it so that a single Client only belongs to one User?
- [x] (BUG) When trying to delete a client, the front-end does not update. Can you improve the experience, so the user knows the client was actually deleted? (tip: use `php artisan db:seed --class=ClientSeeder` to generate some clients if you have none)
- [x] (FEATURE) We noticed users started entering random data when creating clients. We should include some validation. Make sure that, when creating a client:
    - The `name` is up to 190 characters and it's required
    - The `email` is an actual valid email address. Hint: "arunas@example" is NOT a valid email address in our case.
    - The `phone` can only contain digits, spaces and a plus sign
    - At least one of (phone/email) is required
- [x] (FEATURE) The client bookings are currently displayed in random order. Please display them in chronological order, newest first.
- [x] (FEATURE) Users want a quick way to see future and past bookings. When viewing client bookings, can you make a dropdown with three values - "All bookings", "Future bookings only" and "Past bookings only". Make it so that selecting an item from the dropdown would only show bookings that apply to the selected filter. When the page loads, default to "All bookings".

**BONUS TASKS!**
- [x] *BONUS:* (FEATURE) Users have requested the ability to write journals for their clients. A Journal should have a date field (without hours/minutes) and a text field (unlimited length). A client can have many journals. A user should be able to view, create and delete journals.
- [x] *BONUS:* (REFACTOR) We strive for fast and readable code that follows Laravel's/Vue.js style and best practices. Take the time remaining and refactor any code you think can be improved, including ours. The goal is to leave the code better than you found it ;)

**SOME EXTRA**
- [x] Vue3
- [x] prettier
- [X] eslint
