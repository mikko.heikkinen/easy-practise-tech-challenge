<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable = [
        'client_id',
        'start',
        'end',
        'notes',
    ];

    protected $dates = [
        'start',
        'end',
    ];

    public function getStartAttribute($value)
    {
        return Carbon::parse($value)->format('l j F Y, H:i');
    }

    public function getEndAttribute($value)
    {
        return Carbon::parse($value)->format('H:i');
    }
}
