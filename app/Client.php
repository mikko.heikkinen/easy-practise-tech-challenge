<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'name',
        'email',
        'phone',
        'address',
        'city',
        'postcode',
    ];

    protected $appends = [
        'url',
    ];

    public function bookings()
    {
        $booking_filter = request()->bookings;

        $query = $this
            ->hasMany(Booking::class)
            ->orderByDesc('start');

        if ($booking_filter == 'future') {
            $query->whereDate('start', '>', now());
        } elseif ($booking_filter == 'past') {
            $query->whereDate('start', '<', now());
        }

        return $query;
    }

    public function getUrlAttribute()
    {
        return "/clients/" . $this->id;
    }
}
