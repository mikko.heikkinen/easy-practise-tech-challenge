<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Client;
use Illuminate\Http\JsonResponse;

class BookingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param int $client_id
     * @return JsonResponse
     */
    public function index(int $client_id): JsonResponse
    {
        $client = Client::where('id', $client_id)->first();

        return response()->json($client->bookings);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $client_id
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $client_id, int $id): JsonResponse
    {
        $booking = Booking::where('client_id', $client_id)->findOrFail($id);
        $booking->delete();

        return response()->json(['message' => 'Booking deleted successfully']);
    }
}
