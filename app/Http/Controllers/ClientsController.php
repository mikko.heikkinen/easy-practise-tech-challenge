<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClientsController extends Controller
{
    public function index()
    {
        $clients = Client::withCount('bookings')
            ->where('user_id', Auth::id())
            ->get();

        return view('clients.index', ['clients' => $clients]);
    }

    public function create()
    {
        return view('clients.create');
    }

    public function show($client)
    {
        $client = Client::where('id', $client)->first();

        return view('clients.show', [
            'client' => $client
        ]);
    }

    public function store(Request $request): Client
    {
        $validated = $request->validate([
            'name' => 'required|max:190',
            'email' => 'nullable|email:rfc,dns|required_without_all:phone',
            'phone' => 'nullable|regex:/^[\d\s+]+$/|required_without_all:email'
        ]);

        $client = new Client;
        $client->name = $request->get('name');
        $client->email = $request->get('email');
        $client->phone = $request->get('phone');
        $client->address = $request->get('address');
        $client->city = $request->get('city');
        $client->postcode = $request->get('postcode');
        $client->user_id = Auth::id();
        $client->save();

        return $client;
    }

    public function destroy($client): string
    {
        Client::where('id', $client)->delete();

        return 'Deleted';
    }
}
