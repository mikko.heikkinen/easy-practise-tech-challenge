<?php

namespace App\Http\Controllers;

use App\Journal;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class JournalsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $client
     * @return JsonResponse
     */
    public function index($client): JsonResponse
    {
        $journals = Journal::where('client_id', $client)->get();
        return response()->json($journals);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Journal
     */
    public function store(Request $request): Journal
    {
        $request->validate([
            'date' => 'required|date',
            'journal' => 'required',
        ]);

        return Journal::create($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $client_id
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $client_id, int $id): JsonResponse
    {
        $journal = Journal::where('client_id', $client_id)->findOrFail($id);
        $journal->delete();

        return response()->json(['message' => 'Journal deleted successfully']);
    }
}
