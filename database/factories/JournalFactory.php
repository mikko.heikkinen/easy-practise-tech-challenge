<?php

namespace Database\Factories;

use App\Client;
use Illuminate\Database\Eloquent\Factories\Factory;

class JournalFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        // Retrieve an existing client ID from the 'clients' table
        $clientIds = Client::pluck('id');
        $clientId = $this->faker->randomElement($clientIds);

        return [
            'date' => $this->faker->date(),
            'journal' => $this->faker->paragraph(),
            'client_id' => $clientId,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
