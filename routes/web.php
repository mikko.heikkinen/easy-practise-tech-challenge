<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->prefix('clients')->group(function () {
    Route::get('/', 'ClientsController@index')->name('clients.index');
    Route::get('/create', 'ClientsController@create');
    Route::post('/', 'ClientsController@store');
    Route::get('/{client}', 'ClientsController@show');
    Route::delete('/{client}', 'ClientsController@destroy');

    Route::get('/{client_id}/bookings', 'BookingsController@index');
    Route::delete('/{client_id}/bookings/{id}', 'BookingsController@destroy');

    Route::get('/{client}/journals', 'JournalsController@index');
    Route::post('/{client}/journals', 'JournalsController@store');
    Route::delete('/{client}/journals/{journal}', 'JournalsController@destroy');
});
